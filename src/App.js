import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'

import { Home } from './pages/Home'
import { Detail } from './pages/Detail'
import { NotFound } from './pages/NotFound'

import './App.css';
import 'bulma/css/bulma.css'

class App extends Component {
  render() {
    // const url = new URL(document.location)
    // const Page = url.searchParams.has('id')
    //   ? <Detail id={url.searchParams.get('id')}/>
    //   : <Home />
      
    /**
     * Al incluir la última Route y no indicarle ningú path le estamos indicando a React Router
     * que esta siempre se tiene que cargar siempre que el usuario no haya acudido a ninguna ruta anterior
    */

    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/detail/:id' component={Detail} />
          <Route component={NotFound} /> 
        </Switch>
      </div>
    );
  }
}

export default App;
