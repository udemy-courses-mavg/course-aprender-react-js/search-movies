import React from 'react';

export const Title = ({ children }) => (
  <h1 className="title">{children}</h1>
)

/**
 * export default (props) => (
 *  <h1 class="title">{props.title}</h1>
 * )
 */

/**
 * <Title title = 'Search Movies' />
 * <Title>Search Movies </Title>
 * 
 * export default ({ title }) => (
 *  <h1 class="title">{title}</h1>
 * )
 */

/**
 * import whatever from './component/Title'
 * import { Title } from './component/Title'
 * 
 * export default ({ children }) => (
 *  <h1 class="title">{children}</h1>
 * )
 */

